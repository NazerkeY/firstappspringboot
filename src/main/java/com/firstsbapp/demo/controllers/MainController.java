package com.firstsbapp.demo.controllers;

import com.firstsbapp.demo.models.Users;
import com.firstsbapp.demo.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
public class MainController {
    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("title", "Главная страница");
        return "home";
    }

    @GetMapping("/users/add")
    public String addUser(Model model){
        return "addUser";
    }

    @PostMapping("users/add")
    public String addUserPost(@RequestParam String firstName, @RequestParam String lastName,
                              @RequestParam String patronymic, @RequestParam @DateTimeFormat(pattern = "MM/dd/yyyy") Date birthDate,
                              Model model){
        Iterable<Users> users = usersRepository.findAll();
        for(Users user: users){
            if(user.getFirstName().equals(firstName)){
                if(user.getLastName().equals(lastName)){
                    if(user.getPatronymic().equals(patronymic)){
                        if(user.formatDate(user.getBirthDate()).equals(user.formatDate(birthDate))){
                            return "redirect:/users";
                        }
                        else{ break; }
                    }
                    else{ break; }
                }
                else{ break; }
            }
            else{ break; }
        }
        Users created_user = new Users(firstName, lastName, patronymic, birthDate);
        usersRepository.save(created_user);
        return "redirect:/users";
    }

    @GetMapping("/users")
    public String viewUsers(Model model){
        Iterable<Users> users = usersRepository.findAll();
        model.addAttribute("users", users);
        return "viewUsers";
    }
}
