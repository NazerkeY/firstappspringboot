package com.firstsbapp.demo.repositories;

import com.firstsbapp.demo.models.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface UsersRepository extends CrudRepository<Users, Long> { }
